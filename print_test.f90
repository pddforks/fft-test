module pippo1 
contains
!------------------------------------------------------------------------

SUBROUTINE read_rhog ( filename, ngm_g, nspin, rho_g, mill_g, b1, b2, b3, gamma_only, ierr )
   !------------------------------------------------------------------------
   !! Read and distribute rho(G) from file  "filename".* 
   !! (* = dat if fortran binary, * = hdf5 if HDF5)
   !! Processor "root_in_group" reads from file, distributes to
   !! all processors in the intra_group_comm communicator 
      !
USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
      !
      IMPLICIT NONE
      !
      CHARACTER(LEN=*), INTENT(IN)              :: filename
      !! name of file read (to which a suffix is added)
      INTEGER,          INTENT(OUT)              :: ngm_g
      INTEGER,          INTENT(OUT)              :: nspin
      !! read up to nspin components
      COMPLEX(8), ALLOCATABLE, INTENT(INOUT)   :: rho_g(:)
      !! temporary check while waiting for more definitive solutions
      INTEGER, ALLOCATABLE, INTENT(INOUT)       :: mill_g(:,:)
      LOGICAL, OPTIONAL, INTENT(INOUT)             :: gamma_only
      !! if present, don't stop in case of open error, return a nonzero value
      INTEGER, OPTIONAL, INTENT(OUT)            :: ierr
      !
      REAL(dp), INTENT(OUT)                      :: b1(3), b2(3), b3(3)
      INTEGER                                   :: iun, ns, ig
      !
      WRITE( *, *) '(inside read rhog)'
      !
      iun  = 4
      IF ( PRESENT(ierr) ) ierr = 0
      !
      !
      OPEN ( UNIT = iun, FILE = TRIM( filename ) // '.dat', &
           FORM = 'unformatted', STATUS = 'old', iostat = ierr )
      IF ( ierr /= 0 ) THEN
         ierr = 1
         GO TO 10
      END IF
      READ (iun, iostat=ierr) gamma_only, ngm_g, nspin          ! reading GAMMA, NGM, SPIN
      !
      PRINT *, gamma_only, ngm_g, nspin
      !
      IF ( ierr /= 0 ) THEN
         ierr = 2
         GO TO 10
      END IF
      READ (iun, iostat=ierr) b1, b2, b3
!      PRINT *, b1, b2, b3
      IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING B1 B2 B3'
10    CONTINUE 
      !
      !
      ALLOCATE( rho_g(ngm_g) )
      !
      ALLOCATE (mill_g(3,ngm_g))
      !
      !
      READ (iun, iostat=ierr) mill_g(1:3,1:ngm_g)
      !
      DO ns = 1, nspin
         !
         READ (iun, iostat=ierr) rho_g(1:ngm_g)
         !
!         IF ( ierr > 0 ) CALL errore ( 'read_rhog','error reading file ' &
!              & // TRIM( filename ), 2+ns )
         !
         ! 
!         IF ( nspin_ == 2 .AND. nspin == 4 .AND. ns == 2) THEN 
!            DO ig = 1, ngm 
!               rho(ig, 4 ) = rho(ig, 2) 
!               rho(ig, 2 ) = cmplx(0.d0,0.d0, KIND = DP) 
!               rho(ig, 3 ) = cmplx(0.d0,0.d0, KIND = DP)
!            END DO
!         END IF
      END DO
      !
      !
      CLOSE (UNIT = iun, status ='keep' )
      !
      RETURN
      !
END SUBROUTINE read_rhog
end module pippo1
!
!
!
!
!-----------------------------------------------------------------------
PROGRAM print_test_charge_density
  !-----------------------------------------------------------------------
  !
  !
  USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
  use pippo1, only: read_rhog
  !
  IMPLICIT NONE
  !
!NEW ALLOCATED VARIABILE
  INTEGER                   :: ngm_g
  INTEGER                   :: nspin
  COMPLEX(dp), ALLOCATABLE  :: rho_g(:), rho2_g(:)
  COMPLEX(dp), ALLOCATABLE  :: psic(:)
  INTEGER, ALLOCATABLE      :: mill_g(:,:), mill2_g(:,:)
  LOGICAL                   :: gamma_only = .TRUE., is_in_mesh= .FALSE.
  INTEGER                   :: ierr = 0
  INTEGER                   :: un, i, ig, countg
  INTEGER                   :: NR2B2, NR1B2, NR3B2
  REAL(dp)                  :: gcutm
  REAL(dp)                  :: b1(3), b2(3), b3(3)
  REAL(dp)                  :: a(3,3)
  REAL(dp)                  :: bg(3,3)
  REAL(dp), ALLOCATABLE     :: g(:,:)
  !
  REAL(dp)                  :: cutm  ! printing cutoff
  REAL(dp)                  :: r1, r2 
  !
  ! ... now read the charge desity and wave function parameters from a .dat file
  !
  CALL read_rhog ( "charge-density", ngm_g, nspin, rho_g, mill_g, b1, b2, b3, gamma_only, ierr )
  !
  !
  IF ( ierr > 0 ) WRITE(*,*) 'read_rhog:error function'
  !
  !
  CALL read_rhog ( "output-charge-2", ngm_g, nspin, rho2_g, mill2_g, b1, b2, b3, gamma_only, ierr )
  !
  IF ( ierr > 0 ) WRITE(*,*) 'read_rhog:error function'
  !
  cutm=1.d0/1000000.d0
  WRITE (*,*) 'CUTOFF ON OUTPUT VALUES:', cutm
  !
  WRITE(*,*) 'PRINTING CHARGE DENSITY ON .76'
  WRITE(*,*) 'PRINTING OUTPUT CHARGE DENSITY ON .86'
  DO i = 1, 100
      !
      !
      r1 = abs( rho_g(i) )
      r2 = abs( rho2_g(i) )
      !
      IF(r1 .gt. cutm ) THEN
              WRITE(76, *) r1
      ELSE
              WRITE(76, *) 0.d0
      END IF
      !
      !    
      IF(r2 .gt. cutm ) THEN
              WRITE(86, *) r2
      ELSE
              WRITE(86, *) 0.d0
      END IF    
      !
      !
      WRITE (*,*) abs(r1-r2)
      !
      !
  END DO
  !
  !
  DEALLOCATE( rho_g ) 
  DEALLOCATE( rho2_g ) 
  DEALLOCATE( mill_g )
  DEALLOCATE( mill2_g )
  !
  !
END PROGRAM print_test_charge_density
!
