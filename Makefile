# Makefile for fftest
# For the moment it should be compiled in a folder inside q-e. For example inside: q-e/test-fft/
#
# mpif90 -g -D__MPI -cpp -fcheck=bounds fft_test.f90 -o fft_test.x ../Modules/mp_wave.o -I ../FFTXlib -I ../Modules -I ../UtilXlib  ../FFTXlib/libqefft.a -lfftw3 ../UtilXlib/libutil.a

IDIR = ../q-e/

CC = mpif90

CFLAGS = -g -cpp -D__MPI -fcheck=bounds

INCLUDES = \
 -I$(IDIR)FFTXlib \
 -I$(IDIR)UtilXlib\
 -I$(IDIR)Modules

OBJS = fft_test.o \
$(IDIR)Modules/mp_wave.o

LIBS = -lfftw3 

MAIN = fft_test

LFLAGS = $(IDIR)FFTXlib/libqefft.a \
$(IDIR)UtilXlib/libutil.a 


all:	$(MAIN)

deafult:	$(MAIN)

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

$(MAIN).o: $(MAIN).f90 
	$(CC) $(CFLAGS) $(INCLUDES) -c $(MAIN).f90

.PHONY: clean

clean :
	- /bin/rm -f *.x *.o *.a *~ *_tmp.f90 *.d *.mod *.i *.L

# DO NOT DELETE
