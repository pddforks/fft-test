!------------------------------------------------------------------------
module tools
!------------------------------------------------------------------------
 !
 contains
 !
SUBROUTINE read_rhog ( filename, ngm_g, nspin, rho_g, mill_g, b1, b2, b3, gamma_only, ierr )
   !------------------------------------------------------------------------
   !! Read and distribute rho(G) from file  "filename".* 
   !! (* = dat if fortran binary, * = hdf5 if HDF5)
   !! Processor "root_in_group" reads from file, distributes to
   !! all processors in the intra_group_comm communicator 
      !
USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
      !
      IMPLICIT NONE
      !
      CHARACTER(LEN=*), INTENT(IN)              :: filename
      !! name of file read (to which a suffix is added)
      INTEGER,          INTENT(OUT)              :: ngm_g
      INTEGER,          INTENT(OUT)              :: nspin
      !! read up to nspin components
      COMPLEX(dp), ALLOCATABLE, INTENT(INOUT)   :: rho_g(:)
      !! temporary check while waiting for more definitive solutions
      INTEGER, ALLOCATABLE, INTENT(INOUT)       :: mill_g(:,:)
      LOGICAL, OPTIONAL, INTENT(INOUT)             :: gamma_only
      !! if present, don't stop in case of open error, return a nonzero value
      INTEGER, OPTIONAL, INTENT(OUT)            :: ierr
      !
      REAL(dp), INTENT(OUT)                      :: b1(3), b2(3), b3(3)
      INTEGER                                    :: iun, ns, ig
      logical                                    :: gamma_only_ 
      !
      WRITE( *, *) '(inside read rhog)'
      !
      iun  = 4
      IF ( PRESENT(ierr) ) ierr = 0
      !
      !
      OPEN ( UNIT = iun, FILE = TRIM( filename ) // '.dat', &
           FORM = 'unformatted', STATUS = 'old', iostat = ierr )
      IF ( ierr /= 0 ) THEN
         ierr = 1
         GO TO 10
      END IF
      !
      ! ...  READING GAMMA_ONLY, NGM, SPIN
      !
      READ (iun, iostat=ierr) gamma_only_, ngm_g, nspin          
      !
      if (gamma_only_ ) then 
        gamma_only = .true.
      else 
        gamma_only = .false. 
      end if 
      PRINT *, gamma_only, ngm_g, nspin
      !
      IF ( ierr /= 0 ) THEN
         ierr = 2
         GO TO 10
      END IF
      !
      READ (iun, iostat=ierr) b1, b2, b3
      IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING B1 B2 B3'
      !
10    CONTINUE 
      !
      ALLOCATE( rho_g(ngm_g) )
      ALLOCATE (mill_g(3,ngm_g))
      !
      READ (iun, iostat=ierr) mill_g(1:3,1:ngm_g)
      IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING MILL_G'
      !
      DO ns = 1, nspin
         !
         READ (iun, iostat=ierr) rho_g(1:ngm_g)
         IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING RHO_G'
         !
!         IF ( ierr > 0 ) CALL errore ( 'read_rhog','error reading file ' &
!              & // TRIM( filename ), 2+ns )
         !
         ! 
!         IF ( nspin_ == 2 .AND. nspin == 4 .AND. ns == 2) THEN 
!            DO ig = 1, ngm 
!               rho(ig, 4 ) = rho(ig, 2) 
!               rho(ig, 2 ) = cmplx(0.d0,0.d0, KIND = DP) 
!               rho(ig, 3 ) = cmplx(0.d0,0.d0, KIND = DP)
!            END DO
!         END IF
      END DO
      !
      CLOSE (UNIT = iun, status ='keep' )
      !
      RETURN
      !
END SUBROUTINE read_rhog
    !
    !
    !
SUBROUTINE write_rhog ( filename, b1, b2, b3, gamma_only, mill, rho, ecutrho )
      !------------------------------------------------------------------------
      !! Collects rho(G), distributed on "intra_group_comm", writes it
      !! together with related information to file "filename".*
      !! (* = dat if fortran binary, * = hdf5 if HDF5)
      !! Processor "root_in_group" collects data and writes to file
      !
USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
      !
      IMPLICIT NONE
      !
      CHARACTER(LEN=*), INTENT(IN) :: filename
      !! rho(G) is distributed over this group of processors
      REAL(8),         INTENT(IN) :: b1(3), b2(3), b3(3)
      !!  b1, b2, b3 are the three primitive vectors in a.u.
      INTEGER,          INTENT(IN) :: mill(:,:)
      !! Miller indices for local G-vectors
      !! G = mill(1)*b1 + mill(2)*b2 + mill(3)*b3
      LOGICAL,          INTENT(IN) :: gamma_only
      !! if true, only the upper half of G-vectors (z >=0) is present
      COMPLEX(8),      INTENT(IN) :: rho(:)
      !! rho(G) on this processor
      REAL(8),OPTIONAL,INTENT(IN) :: ecutrho
      !! cut-off parameter for G-vectors, only the one in root node is
      !! used, hopefully the same as in the other nodes.  
      !
      INTEGER                  :: ngm, nspin, ns
      INTEGER                  :: iun, ierr
      !
      ngm  = SIZE (rho, 1)
!      IF (ngm /= SIZE (mill, 2) ) &
!         CALL errore('write_rhog', 'inconsistent input dimensions', 1)
      !
      nspin= 1 !SIZE (rho, 2)
      iun  = 6
      !
      ! ... find out the global number of G vectors: ngm
      !
      ierr = 0
      !
      OPEN ( UNIT = iun, FILE = TRIM(filename)//'.dat', &
                FORM = 'unformatted', STATUS = 'unknown', iostat = ierr )
      IF ( ierr /= 0 ) WRITE(*,*) 'error in opening the output file'
      !
      !
      WRITE (iun, iostat=ierr) gamma_only, ngm, nspin
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing gamma_only, ngm, nspin'
      !
      WRITE (iun, iostat=ierr) b1, b2, b3
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing b1, b2, b3'
      !
      ! ... write mill_g
      !
      WRITE (iun, iostat=ierr) mill(1:3,1:ngm)
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing mill_g'
      !
      ! ... write rho_g
      !
      DO ns = 1, nspin
         !
         WRITE (iun, iostat=ierr) rho(1:ngm)
         IF ( ierr /= 0 ) WRITE(*,*) 'not writing b1, b2, b3'
         !
      END DO
      !
      CLOSE (UNIT = iun, status ='keep' )
      !
      RETURN
      !
END SUBROUTINE write_rhog
    !
    !
    !
SUBROUTINE recips (a1, a2, a3, b1, b2, b3)
    !---------------------------------------------------------------------
    !
    !   This routine generates the reciprocal lattice vectors b1,b2,b3
    !   given the real space vectors a1,a2,a3. The b's are units of 2 pi/a.
    !
    !     first the input variables
    !
USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
    !
    !
    implicit none
    !
    real(DP), INTENT (IN) :: a1 (3), a2 (3), a3 (3)
    REAL(DP), INTENT (OUT)  :: b1 (3), b2 (3), b3 (3)
    ! input: first direct lattice vector
    ! input: second direct lattice vector
    ! input: third direct lattice vector
    ! output: first reciprocal lattice vector
    ! output: second reciprocal lattice vector
    ! output: third reciprocal lattice vector
    !
    !   then the local variables
    !
    real(DP) :: den, s
    ! the denominator
    ! the sign of the permutations
    integer :: iperm, i, j, k, l, ipol
    ! counter on the permutations
    !\
    !  Auxiliary variables
    !/
    !
    ! Counter on the polarizations
    !
    !    first we compute the denominator
    !
    den = 0
    i = 1
    j = 2
    k = 3
    s = 1.d0
  100 do iperm = 1, 3
       den = den + s * a1 (i) * a2 (j) * a3 (k)
       l = i
       i = j
       j = k
       k = l
    enddo
    i = 2
    j = 1
    k = 3
    s = - s
    if (s.lt.0.d0) goto 100
    !
    ! ... reciprocal vectors
    !
    i = 1
    j = 2
    k = 3
    do ipol = 1, 3
       b1 (ipol) = (a2 (j) * a3 (k) - a2 (k) * a3 (j) ) / den
       b2 (ipol) = (a3 (j) * a1 (k) - a3 (k) * a1 (j) ) / den
       b3 (ipol) = (a1 (j) * a2 (k) - a1 (k) * a2 (j) ) / den
       l = i
       i = j
       j = k
       k = l
    enddo
    return
    !
end subroutine recips
 !
!----------------------------------------------------------------------
end module tools
!----------------------------------------------------------------------
!
!
!-----------------------------------------------------------------------
PROGRAM test_charge_density
  !-----------------------------------------------------------------------
  !
  !
  USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
  use tools, only: read_rhog, write_rhog, recips
  USE fft_types, only: fft_type_descriptor, fft_type_init, fft_stick_index
  USE stick_base, only: sticks_map
  USE fft_interfaces, ONLY: invfft, fwfft
  USE fft_ggen, ONLY : fft_set_nl
  !
  !... para libraries
  !
  USE mpi
  USE mp,         ONLY : mp_size, mp_rank, mp_bcast, mp_sum, mp_start, mp_barrier
  USE mp_wave,    ONLY : splitwf, mergekg, mergewf
  !
  IMPLICIT NONE
  !
!NEW ALLOCATED VARIABILE
  INTEGER                   :: ngm_g, ngm_g_new, ngm2_g
  INTEGER                   :: ngm_offset, ngm2_offset
  INTEGER                   :: nspin
  COMPLEX(dp), ALLOCATABLE  :: rho_g(:), rho(:)
  COMPLEX(dp), ALLOCATABLE  :: psic(:)
  INTEGER, ALLOCATABLE      :: mill_g(:,:), mill(:,:), mill2(:,:)
  INTEGER, ALLOCATABLE      :: ig_l2g(:), ig_l2g_2(:)
  INTEGER, ALLOCATABLE      :: ngmpe(:), ngmpe2(:)
  LOGICAL                   :: gamma_only = .TRUE., is_in_mesh= .FALSE.
  INTEGER                   :: ierr = 0
  INTEGER                   :: un, i, ig, countg, countg_g
  INTEGER                   :: ng, ngm
  INTEGER                   :: NR2B2, NR1B2, NR3B2
  REAL(dp)                  :: gcutm
  REAL(dp)                  :: b1(3), b2(3), b3(3)
  REAL(dp)                  :: a(3,3)
  REAL(dp)                  :: bg(3,3)
  REAL(dp)                  :: gmax(3)
  REAL(dp), ALLOCATABLE     :: g(:,:)
  !
  !... FFT descriptors
  !
  TYPE(fft_type_descriptor) :: dfftp, dfftp2
  TYPE(sticks_map) :: smap, smap2
  !
  ! ... para variables 
  !
  INTEGER :: mype, npes, comm, root
  !! MPI handles
  INTEGER :: ntgs
  !! number of taskgroups
  INTEGER :: nbnd
  !! number of bands
  LOGICAL :: iope
  !!
  LOGICAL :: lpara, is_local
  !
  !
  !
#if defined(__MPI)
  CALL MPI_Init(ierr)
  CALL mpi_comm_rank(MPI_COMM_WORLD, mype, ierr)
  CALL mpi_comm_size(MPI_COMM_WORLD, npes, ierr)
  call mp_start(npes, mype, mpi_comm_world) 
  comm = MPI_COMM_WORLD
  root = 0
  IF (mype == root) THEN
    iope = .true.
  ELSE
    iope = .false.
  ENDIF
  lpara=.true. 
#else
  mype = 0
  npes = 1
  comm = 0
  ntgs = 1
  root = 0
  iope = .true.
  lpara=.false.
#endif
  !
  IF( mype == 0 ) WRITE (*,*) 'npes= ', npes
  !
  ! ... now read the charge density and wave function parameters from a .dat file
  !
  IF ( mype == 0 ) THEN
  !
     CALL read_rhog ( "charge-density", ngm_g, nspin, rho_g, mill_g, b1, b2, b3, gamma_only, ierr )
  !   CALL read_rhog ( "output-charge", ngm_g, nspin, rho_g, mill_g, b1, b2, b3, gamma_only, ierr )
     IF ( ierr > 0 ) WRITE(*,*) 'read_rhog:error function'
  END IF 
  !
#if defined(__MPI)
  !
  CALL MPI_BCAST(ngm_g, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
  CALL MPI_BCAST(nspin, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
  CALL MPI_BCAST(gamma_only, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
  CALL MP_BCAST(b1, 0, MPI_COMM_WORLD)
  CALL MP_BCAST(b2, 0, MPI_COMM_WORLD)
  CALL MP_BCAST(b3, 0, MPI_COMM_WORLD)
  !
#endif
  !
  IF (mype == 0) print *, 'rho_g', rho_g(1) 
  !
  IF ( mype .ne. 0)THEN
     ALLOCATE (mill_g(3,ngm_g))
     ALLOCATE( rho_g(1) )
  END IF
  !
  CALL MP_BCAST(mill_g, 0, MPI_COMM_WORLD)
  !
  ! ...  Print b parameters 
  !
  IF ( mype== 0) THEN
     !
     PRINT *, b1  
     PRINT *, b2
     PRINT *, b3
     !
  END IF 
  !
  bg(:,1)=b1
  bg(:,2)=b2
  bg(:,3)=b3
  !
  !
  ! ... g is the array for the grid indices
  !
  !
  IF ( mype == 0) THEN
     ! 
     gmax(1:3) = mill_g(1,ngm_g) * bg(:, 1) + mill_g(2,ngm_g) * bg(:, 2) + mill_g(3,ngm_g) * bg(:, 3)
     !
     gcutm = dot_product(gmax(:),gmax(:)) + 0.001
     !
     !
     PRINT *, GCUTM
     !
     nr1b2 = MAXVAL(ABS(mill_g(1,:)))
     nr2b2 = MAXVAL(ABS(mill_g(2,:)))
     nr3b2 = MAXVAL(ABS(mill_g(3,:)))
     !
     PRINT * , nr1b2, nr2b2, nr3b2
     !
  END IF
  !
  CALL MPI_BCAST(gcutm, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
  !
  ! ... fft_type_init per inizializzare la griglia
  !
  CALL recips( b1, b2, b3, a(:,1), a(:,2), a(:,3))
  !
  a= a * 8.d0 * atan(1.d0)
  !
  !
  !
  dfftp%rho_clock_label='fftp'
  dfftp2%rho_clock_label='fftp2'
  !
  CALL fft_type_init(dfftp, smap, "rho", gamma_only, lpara, comm, a, bg, gcutm, 4.d0, nyfft=1, nmany=1)
  !
  CALL fft_type_init(dfftp2, smap2, "rho", .not. gamma_only, lpara, comm, a, bg, gcutm, 4.d0, nyfft=1, nmany=1)
  !
  IF ( mype == 0) THEN
     WRITE (*,*) mype, 'DFFTP: nr1 = ', dfftp%nr1
     WRITE (*,*) mype, '       nr2 = ', dfftp%nr2
     WRITE (*,*) mype, '       nr3 = ', dfftp%nr3
  END IF
  !
  ! ... allocate local mill 
  !
  !
  !
  ALLOCATE( ngmpe( npes ) )
  ALLOCATE( ngmpe2( npes ) )
  !
  ngmpe  = 0
  ngmpe2 = 0
  !
  ngmpe( mype + 1 )  = dfftp%ngm
  ngmpe2( mype + 1 ) = dfftp2%ngm
  !
  call MPI_Barrier( comm, ierr )
  WRITE (*,*) ngmpe, ngmpe2
  !
  CALL mp_sum( ngmpe, comm )
  CALL mp_sum( ngmpe2, comm )
  !
  call MPI_Barrier( comm, ierr )
  WRITE (*,*) 'after mp_sum'
  WRITE (*,*) ngmpe, ngmpe2
  !
  ngm_g_new = sum(ngmpe) 
  ngm2_g = sum(ngmpe2)
  !
  WRITE (*,*) 'ngm_g, ngm_g_new'
  WRITE (*,*) ngm_g, ngm_g_new, ngm2_g
  !
  ngm_offset = 0
  ngm2_offset = 0
  !
  !
  DO ng = 1, mype
     ngm_offset = ngm_offset + ngmpe( ng )
     ngm2_offset = ngm2_offset + ngmpe2( ng )
  END DO
  !
  DEALLOCATE( ngmpe, ngmpe2 )
  !
  print '(I3,"  ngm_g:",I8,XXX,I8,"  ngm2_g:",I8)', mype, ngm_g, ngm_g_new, ngm2_g
  ALLOCATE( ig_l2g( dfftp%ngm), ig_l2g_2( dfftp2%ngm ) )
  !
  ngm = 0
  !
  !
  ! ... split mill_g into mill
  !
  ALLOCATE(mill(3,dfftp%ngm), mill2(3,dfftp2%ngm))
  !
  ngm = 0
  countg= 0
  countg_g= 0
  DO ig = 1,ngm_g
     IF ( fft_stick_index( dfftp, mill_g(1,ig), mill_g(2,ig) ) .gt. 0 ) THEN
        ngm = ngm + 1
        ig_l2g( ngm ) = ig 
        mill(:,ngm) = mill_g(:,ig)
     END IF
     !
     IF (.NOT. gamma_only) THEN
        !
        is_in_mesh = mill_g(1,ig).gt.0
        is_in_mesh = is_in_mesh .OR. ( mill_g(1,ig).eq.0 .AND. mill_g(2,ig).gt.0 )
        is_in_mesh = is_in_mesh .OR. ( mill_g(1,ig).eq.0 .AND. mill_g(2,ig).eq.0 .AND. mill_g(3,ig).ge.0)
        !
        IF(is_in_mesh) countg_g = countg_g + 1
        !
     ELSE
        ! 
        is_in_mesh = ( mill_g(1,ig).eq.0 .AND. mill_g(2,ig).eq.0 .AND. mill_g(3,ig).eq.0)
        countg_g = countg_g +1
        !
     END IF       
     !  
     !
     IF (.not. gamma_only) THEN 
        IF ( fft_stick_index( dfftp2, mill_g(1,ig), mill_g(2,ig) ) .gt. 0 ) THEN 
           !
           IF( is_in_mesh ) THEN
              countg=countg+1
              mill2(:,countg) = mill_g(:,ig)
              ig_l2g_2(countg ) =  countg_g
           END IF           
        END IF
     ELSE
        IF ( fft_stick_index( dfftp2, mill_g(1,ig), mill_g(2,ig) ) .gt. 0 ) THEN
           countg=countg+1
           mill2(:,countg) = mill_g(:,ig)
           ig_l2g_2(countg ) =  countg_g
        END IF
        IF (.not. is_in_mesh) countg_g = countg_g+1 
        IF (.not. is_in_mesh .AND. ( fft_stick_index( dfftp2, -mill_g(1,ig), -mill_g(2,ig) ) .gt. 0 )  ) THEN
           countg   = countg+1 
           mill2(:,countg) = - mill_g(:,ig)
           ig_l2g_2(countg ) =  countg_g
        END IF
     END IF
  END DO 
  print '("check: mpype= ",I0,"   ngm2_g = ",I0,"  countg_g=", I0)', mype, ngm2_g, countg_g
  !
  DEALLOCATE(mill_g)
  ! 
  ! *****************************************************
  !
  ALLOCATE(g(3,dfftp%ngm))
  !
  DO ig=1,dfftp%ngm
  !
     g(1:3,ig) = mill(1,ig) * bg(:, 1) + mill(2,ig) * bg(:, 2) + mill(3,ig) * bg(:, 3)
  !
  END DO
  !
  g = g / 8.d0 / atan(1.d0)
  CALL fft_set_nl( dfftp, a, g) 
  !
  DEALLOCATE(g)
  ALLOCATE(g(3,dfftp2%ngm))
  g = 0.d0
  !
  DO ig=1,dfftp2%ngm
     g(1:3,ig) = mill2(1,ig) * bg(:, 1) + mill2(2,ig) * bg(:, 2) + mill2(3,ig) * bg(:, 3)  
  END DO
  !
  !
  PRINT *, "mype"
  PRINT *, mype
  PRINT *, "dfftp2%ngm"
  PRINT *, dfftp2%ngm
  PRINT *, "countg"
  PRINT *, countg
  !
  g = g / 8.d0 / atan(1.d0)
  !
  PRINT *, dfftp%nproc, dfftp2%nproc
  PRINT *, dfftp%nsp, dfftp2%nsp
  PRINT *, dfftp%ngl, dfftp2%ngl
  PRINT *, dfftp%ngl, dfftp2%ngl
  PRINT *, dfftp%nnr, dfftp2%nnr
  PRINT *, dfftp%ngm, dfftp2%ngm
  !
  DEALLOCATE (mill)
  !
  CALL fft_set_nl( dfftp2, a, g(:,1:dfftp2%ngm))
  !
  ! ...  split rho_g
  !
  ALLOCATE(rho(dfftp%ngm))
  CALL splitwf ( rho, rho_g, dfftp%ngm, ig_l2g, mype, dfftp%nproc, 0, MPI_COMM_WORLD )
  !
  !
  ALLOCATE (psic(dfftp%nnr))
  !
  psic = CMPLX(0.d0,0.d0,KIND=dp)
  !
  !
  DO i = 1, dfftp%ngm
      !
      IF(gamma_only) THEN
              psic(dfftp%nlm(i))=DCONJG(rho(i))
      END IF
      !
      psic(dfftp%nl(i))=rho(i)
      !
  END DO
  !
  !
  CALL invfft('Rho', psic, dfftp)
  !
  CALL fwfft('Rho', psic, dfftp2)
  !
!  CALL invfft('Rho', psic, dfftp2)
  !
!  CALL fwfft('Rho', psic, dfftp)
  !
  !
  DEALLOCATE(rho)
  ALLOCATE(rho(dfftp2%ngm))
  !
  DO i = 1, dfftp2%ngm
      !
      rho(i)=psic(dfftp2%nl(i))
      !
  END DO
  !
  !
  DEALLOCATE(rho_g)
  IF (mype == 0) THEN 
    ALLOCATE(mill_g(3,ngm2_g), rho_g(ngm2_g))
  ELSE 
    ALLOCATE(mill_g(1,1), rho_g(1))
  END IF
  !
  CALL mergekg ( mill2, mill_g, dfftp2%ngm, ig_l2g_2, mype, dfftp2%nproc, 0, MPI_COMM_WORLD )
  !
  CALL mergewf ( rho, rho_g, dfftp2%ngm, ig_l2g_2, mype, dfftp2%nproc, 0, MPI_COMM_WORLD )
  IF (mype == 0) print *, 'rho_g', rho_g(1) 
  !
  !IF ( mype == 0) CALL  write_rhog ( "output-charge-2", b1, b2, b3, .NOT. gamma_only, mill_g(:,1:countg_g), rho_g(1:countg_g) )
  IF ( mype == 0) CALL  write_rhog ( "output-charge", b1, b2, b3, .NOT. gamma_only, mill_g(:,1:countg_g), rho_g(1:countg_g) )
  !
  !
  IF (ALLOCATED(rho_g))  DEALLOCATE( rho_g ) 
  IF (ALLOCATED(mill_g)) DEALLOCATE( mill_g )
  !
  CALL mpi_finalize(ierr)
 !
END PROGRAM test_charge_density
!
